cars = [ //A list of objects each representing a car
    {
        road: [],
        destination: [0],
        p: 0,
        v: 1,
        nextAct: 0,
        props: [],
        color: {
            r: 255,
            g: 0,
            b: 0
        }
    }
]

var red = 255;
var blue = 255;
var green = 255;

function getRandInt(n) {
    return Math.floor(Math.random() * Math.floor(n));
}

function newCarButton() {
    var c = 0;
    while (c <= 150) {
        var r = getRandInt(255);
        var b = getRandInt(255);
        var g = getRandInt(255);
        var c = r + b + g;
    }
    newCar([], 10, r, b, g);
    cars[cars.length - 1].v = 1 + ((getRandInt(8) - 4) * 0.1);
}

function updateCarRoad(car, roadId) {
    car.road = roadId;
}

function newCar(road, p, r, b, g) {
    cars.push({
        road: road,
        destination: [],
        color: {
            r: r,
            b: b,
            g: g
        },
        p: p,
        v: 0,
        nextAct: 0,
        props: []
    })
    id2path(road).cars.push(cars.length - 1);
}

function updatePosition(car) {
    car.p += car.v;
}

function newRoadButton() {
    var l = Number(document.getElementById('newRoadLength').value);
    var d = Number(document.getElementById('newRoadDirection').value);
    makeRoad(latestRoad(0), l, d);
}

function clear() {
    ctx.clearRect(-1000000 + y, -1000000 + x, canvas.width + 1000000000 + y, canvas.height + 100000000 + x)
}

var zoom = 1;

function drawRoad(road, x, y) {
    ctx.translate(0, y * zoom);
    ctx.rotate(road.direction * Math.PI / 180);
    ctx.fillStyle = "black";
    ctx.fillRect(-5 * zoom, 5 * zoom, 10 * zoom, road.length * zoom);
    drawCars(road);
}

function drawCars(road) {
    var i;
    for (i = 0; i < cars.length; i++) {
        if (idMatch(road.id, cars[i].road)) {
            ctx.fillStyle = "rgb(" + cars[i].color.r + "," + cars[i].color.g + "," + cars[i].color.b + ")" //"(cars[i].color.r, cars[i].color.b, cars[i].color.g)";
            ctx.fillRect(-5 * zoom, (cars[i].p + 5) * zoom, 10 * zoom, 10 * zoom);
        }
    }
}

function drawRoads(roads, length) {
    ctx.save();
    drawRoad(roads, 0, length);
    var i;
    for (i = 0; i < roads.children.length; i++) {
        drawRoads(roads.children[i], (roads.length));
    }
    ctx.restore();
}

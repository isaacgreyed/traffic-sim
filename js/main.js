var c = document.getElementById("canvas");
var ctx = c.getContext("2d");
makeRoad(roadTree, 200, -90);
makeRoad(latestRoad(0), 200, -90);
makeRoad(latestRoad(0), 200, -90);
drawRoads(roadTree, 0);
newCar([0], 50, 0, 255, 0);
cars[1].v = 1;
var x = 100;
var y = 100;


document.onkeydown = function (e) {
    e = e || window.event;
    var key = e.which || e.keyCode;
    if (key === 187) {
        zoom += 0.1;
    }
    if (key === 189) {
        zoom -= 0.1;
    }
    if (key === 38) {
        y += 10;
    }
    if (key === 40) {
        y -= 10;
    }
    if (key === 37) {
        x += 10;
    }
    if (key === 39) {
        x -= 10;
    }
}


function removeElement(array, elem) {
    var i;
    for (i = 0; i < array.length; i++) {
        if (array[i] == elem) {
            return array.splice(i, 1);
        }
    }
}


function checkInRange(arr, val, range) {
    return arr.some(function (arrVal) {
        return ((val > arrVal - range) && (val < arrVal + range));
    });
}

function moveToNextRoad(i, currentCarsRoad) {
    if (cars[i].p + 10 > (currentCarsRoad.length)) {
        if (currentCarsRoad.children[0] == undefined) {
            updateCarRoad(cars[i], []);
            roadTree.cars.push(i);
            removeElement(currentCarsRoad.cars, i);
            cars[i].p = -10;
        } else {
            updateCarRoad(cars[i], currentCarsRoad.children[0].id);
            removeElement(currentCarsRoad.cars, i);
            currentCarsRoad.children[0].cars.push(i);
            cars[i].p = -10;
        }
    }
}

function moveCars() {
    var i;
    for (i = 0; i < cars.length; i++) { //Do for all cars
        updatePosition(cars[i]);
        var currentCarsRoad = id2path(cars[i].road);
        moveToNextRoad(i, currentCarsRoad);
    }
}

function checkCars() {
    var i;
    for (i = 0; i < roadList.length; i++) { //Do for all roads
        carList = roadList[i].cars
        if (carList.length > 1) {

            var positions = [];

            var j;
            for (j = 0; j < carList.length; j++) {
                positions.push(cars[carList[j]].p);
            }

            var k;
            for (k = 0; k < carList.length; k++) {
                var cPos = positions[k];
                positions[k] = -50;

                if (checkInRange(positions, cPos, 5)) {
                    console.log("crash");
                    crash = false;
                }

                positions[k] = cPos
            }

            var k;
            for (k = carList.length - 1; k > 0; k--) {
                if (((cars[carList[k]].v * 30) + cars[carList[k]].p) > cars[carList[k - 1]].p) {
                    if (cars[carList[k]].v > 0) {
                        cars[carList[k]].v -= 0.03;
                    } else {
                        cars[carList[k]].v = 0
                    }
                }
            }

        }

        var k;
        for (k = 0; k < carList.length; k++) {
            if (cars[carList[k]].v <= 1.5) {
                cars[carList[k]].v += 0.01
            }
        }

    }
}

function update() {
    checkCars();
    moveCars();
}

function draw() {
    clear();
    drawRoads(roadTree, 0);
}

window.setInterval(function () { //Main Loop
    ctx.save();
    ctx.translate(x, y);
    update();
    draw();
    ctx.restore();
}, 33);
